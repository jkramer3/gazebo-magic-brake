/**
BSD 3-Clause License

Copyright (c) 2021, James Kramer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef GAZEBO_MAGIC_BRAKE_HPP
#define GAZEBO_MAGIC_BRAKE_HPP

#include <tf/tf.h>
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <std_srvs/Empty.h>

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <math.h>

namespace gazebo {
	
	/** Gazebo plugin that maintains an entity's position in the world
	 * after receiving a zero velocity command. This is a workaround for
	 * a situation where a robot that was supposed to be station (with
	 * zero velocity) was slowly &quot;sliding&quot; across the ground. */
	class MagicBrakePlugin : public ModelPlugin {
	public:
		MagicBrakePlugin();
		~MagicBrakePlugin() { }
		void Load(physics::ModelPtr parent, sdf::ElementPtr sdf);
		void OnUpdate(const common::UpdateInfo &info);
		
	private:
		// gazebo sim data
		physics::WorldPtr m_world;
		sdf::ElementPtr m_sdf;
		event::ConnectionPtr m_updateConnection;
		ros::Time m_lastUpdateTime, m_lastCmdVelTime;
		double m_updatePeriod, m_engageTimeout;
		
		// robot data
		std::string m_modelName;
		physics::ModelPtr m_model;
		double m_threshTvel, m_threshRvel; // translation/rotation velocities
		bool m_engaged;
		math::Pose m_pose;
		ros::Subscriber m_velSub;
		// used this for debug; maybe useful to have?
		//ros::ServiceServer m_toggleBrakeSrv;
		
		void cmdVelCb(const geometry_msgs::Twist::ConstPtr &msg);
		bool toggleBrakeCb(std_srvs::Empty::Request &req,
		                   std_srvs::Empty::Response &rsp);
		bool within(double val, double set, double thresh);
		ros::Time getWorldTime();
	};
}
#endif
