# Gazebo Magic Brake

Gazebo plug-in that will maintain an entity's position when its velocity is
zero.

I wrote this after being handed an improperly configured robot representation
for ROS/Gazebo and I could not obtain an update in a timely fashion. To make
a deadline, this was a better choice than spending an indeterminate amount
of time and effort on debugging an unfamiliar and faulty model.

The issue showed itself when moving a robot to a specific location where
some end-effector manipulation was to be performed. Gravity and friction
should have kept the robot in place, but instead it slowly (_very_ slowly)
slid across the ground plane, as if the coefficient of friction was too
low. While the manipulation controller had some amount of built-in flexibility,
after a certain amount of time passed the robot was simply too far away to
even reach its target.

This works by monitoring the `/robot/cmd_vel` topic, engaging the brake
when a velocity of zero (both translational and rotational) is received,
then disengaging on non-zero velocity. Velocity thresholds for zero, the
actual topic, and the entity ID are configured in the xacro file. Using
the `magic_brake.launch` file links the ROS robot and gazebot by both
engaging xacro and appending the &quot;magic brake&quot; to the robot
description on the parameter server.

